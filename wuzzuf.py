from bs4 import BeautifulSoup
import requests


class Job:
    def __init__(self, link, title):
        self.link = link
        self.title = title
        self.company = ""
        self.location = ""

    def __str__(self) -> str:
        return f'Title:     {self.title}\n' + \
               f'Link:      {self.link}\n' \
               f'Company:   {self.company}\n' \
               f'Location:  {self.location}'


def scrap_job(job: Job) -> None:
    request = requests.get(job.link)
    soup = BeautifulSoup(request.content, "lxml")
    app = soup.find("div", {"id": "app"})
    company_section = app.find("section", {"class": "css-1rhgoyg"})

    if company_section is None:
        job.company = "Confidential Company"
    else:
        company_title = company_section.find("h3", {"class": "css-o85roq"})
        job.company = company_title.find("a").text

    location = app.find("strong", {"class": "css-9geu3q"})
    location = location.text.split("-")[1].strip()
    job.location = location


def search(search_term) -> None:
    url = "https://wuzzuf.net/search/jobs/?"
    query_string = {'q': search_term}
    request = requests.get(url, params=query_string)

    jobs = []
    soup = BeautifulSoup(request.content, "lxml")
    app = soup.find("div", {"id": "app"})
    for item in app.find_all("div", {"class": "e1v1l3u10"}):
        header = item.find("h2", {"class": "css-m604qf"})
        if header:
            a = header.find("a")
            if a:
                jobs.append(Job(a["href"], a.text))

    for job in jobs:
        scrap_job(job)

    for job in jobs:
        print(job)
        print("=" * 40)


if __name__ == '__main__':
    search("java")
